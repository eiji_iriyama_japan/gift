$(document).ready(function () {
 $.ajax({
  url: "../gift/find/targetPerson",
  type: "GET",
  dataType: "JSON",
  data: {},
  success: function(data){
   $.each(data, function(i, item){
    $("#target_person").append('<li><a type="button" href="target_person.html?tid=' + item.target_person_id + '" class="btn btn-lg btn-primary">' + item.lastname + ' ' + item.firstname + '</a></li>');
   });
  },
  error: function (XMLHttpRequest, textStatus, errorThrown) {
   alert("エラー: ターゲット人物情報取得に失敗しました" + textStatus + "\n" + errorThrown);
  }
 });
});

$(document).ready(function () {
 $.ajax({
  url: "../gift/find/tweets",
  type: "GET",
  dataType: "JSON",
  data: {},
  success: function(data){
   $.each(data, function(i, tweet){
    $("#twitter_trend").append('<li class="list-group-item">@' + tweet.name + '<br>' + tweet.text + '</li>');
   });
  },
  error: function (XMLHttpRequest, textStatus, errorThrown) {
   alert("エラー: ツイッター取得に失敗しました" + textStatus + "\n" + errorThrown);
  }
 });
});

$(document).ready(function () {
	var tid = decodeURIComponent(location.search.substring(5));
 $.ajax({
  url: "../gift/find/entryThing",
  type: "GET",
  dataType: "JSON",
  data: {"targetPersonId" : tid},
  success: function(data){
   $.each(data, function(i, item){
    $("#thing").append('<li class="list-group-item">' + item.description + '<a href="../gift/remove/entryThing?entryThingId=' + item.entry_thing_id + '"><img class="delete_button" src="../design/img/delete.png"/></a>' + '</li>');
   });
   $("#thing").append(
		   '<form action="../gift/add/entryThing" method="get">' + 
		   '<div class="form-group">' + 
		   '<input type="text" name="description" class="form-control">' + 
		   '</div>' + 
		   '<input type="hidden" name="targetPersonId" value="' + tid + '">' + 
		   '<input type="submit" value="submit">' +
		   '</form>');
  },
  error: function (XMLHttpRequest, textStatus, errorThrown) {
   alert("エラー: 「モノ」取得に失敗しました" + textStatus + "\n" + errorThrown);
  }  
 });
});

$(document).ready(function () {
	var tid = decodeURIComponent(location.search.substring(5));
 $.ajax({
  url: "../gift/find/entryBrand",
  type: "GET",
  dataType: "JSON",
  data: {"targetPersonId" : tid},
  success: function(data){
   $.each(data, function(i, item){
    $("#brand").append('<li class="list-group-item">' + item.description + '<a href="../gift/remove/entryBrand?entryBrandId=' + item.entry_brand_id + '"><img class="delete_button" src="../design/img/delete.png"/></a>' + '</li>');
   });
   $("#brand").append(
		   '<form action="../gift/add/entryBrand" method="get">' + 
		   '<div class="form-group">' + 
		   '<input type="text" name="description" class="form-control">' + 
		   '</div>' + 
		   '<input type="hidden" name="targetPersonId" value="' + tid + '">' + 
		   '<input type="submit" value="submit">' +
		   '</form>');
  },
  error: function (XMLHttpRequest, textStatus, errorThrown) {
   alert("エラー: 「ブランド」取得に失敗しました" + textStatus + "\n" + errorThrown);
  }
 });
});
$(document).ready(function () {
	var tid = decodeURIComponent(location.search.substring(5));
 $.ajax({
  url: "../gift/find/entryPlan",
  type: "GET",
  dataType: "JSON",
  data: {"targetPersonId" : tid},
  success: function(data){
   $.each(data, function(i, item){
    $("#plan").append('<li class="list-group-item">' + item.description + '<a href="../gift/remove/entryPlan?entryPlanId=' + item.entry_plan_id + '"><img class="delete_button" src="../design/img/delete.png"/></a>' + '</li>');
   });
   $("#plan").append(
		   '<form action="../gift/add/entryPlan" method="get">' + 
		   '<div class="form-group">' + 
		   '<input type="text" name="description" class="form-control">' + 
		   '</div>' + 
		   '<input type="hidden" name="targetPersonId" value="' + tid + '">' + 
		   '<input type="submit" value="submit">' +
		   '</form>');
  },
  error: function (XMLHttpRequest, textStatus, errorThrown) {
   alert("エラー: 「予定」取得に失敗しました" + textStatus + "\n" + errorThrown);
  }
 });
});
