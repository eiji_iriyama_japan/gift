package org.iriyama.gift.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * ログインの成立を確認するフィルター<br>
 * ログインしていない場合、ログイン画面へ遷移。
 * @author eijis
 *
 */
public class LoginFilter implements Filter{
	private static final Logger LOG = LogManager.getLogger(LoginFilter.class);
	private static final String USER_SESSION ="USER_SESSION";
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		HttpSession session = httpServletRequest.getSession(false);
		if (session == null) {
			LOG.info("ログイン出来ておりません。ログインをしてください。");
			httpServletResponse.sendRedirect("/public/login.html");
			return;
		}
		
		Object user = session.getAttribute(USER_SESSION);
		if (user == null) {
			LOG.info("ログインをしてください。");
			httpServletResponse.sendRedirect("/public/login.html");
			return;
		} else {
			//TODO セッション内のaccountIdがDBに存在していることを確認
			LOG.info("ログインをしてます: " + user.toString());
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

}
