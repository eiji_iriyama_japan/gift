package org.iriyama.gift.filter;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Provider
@Priority(Priorities.AUTHENTICATION)
public class LoginFilterJax implements ContainerRequestFilter, ContainerResponseFilter{
	private static final Logger LOG = LogManager.getLogger(LoginFilterJax.class);

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		LOG.info("jaxfilter");
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		//TODO ログインフィルターをJaxrsのものと入れ替える
	}

}
