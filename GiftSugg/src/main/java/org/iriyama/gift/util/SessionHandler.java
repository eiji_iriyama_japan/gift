package org.iriyama.gift.util;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Context;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * セッションとHttpServletRequest/Responseについて扱います。
 * @author eijis
 *
 */

//TODO DIからセッションを使う(application クラス)
public class SessionHandler {
	private static final Logger LOG = LogManager.getLogger(SessionHandler.class);
	protected static final String USER_SESSION ="USER_SESSION";
	@Context protected HttpServletRequest request;
	@Context protected HttpServletResponse response;
	
	protected int getAccountId() {
		HttpSession session = request.getSession(false);
		if (session == null) {
			LOG.info("セッションがありません");
			return 0;
		} else {
			int accountId = (int)session.getAttribute(USER_SESSION);
			LOG.info("USER_SESSION: " + accountId);
			return accountId;
		}
	}

	protected void setSendRedirect(String url) {
		try {
			response.sendRedirect(url);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			//TODO 失敗時に、決まったURLへシステムエラーページへリダイレクト
		}
	}
	
	protected void invalidateSession(HttpSession session) {
		session.removeAttribute(USER_SESSION);
		session.invalidate();
	}
}
