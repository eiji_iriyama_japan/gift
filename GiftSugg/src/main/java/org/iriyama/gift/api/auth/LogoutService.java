package org.iriyama.gift.api.auth;

import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.iriyama.gift.util.SessionHandler;
/**
 * ログアウト処理をします<br>
 * 成功時にlogin.htmlに遷移<br>。
 * @author eijis
 *
 */

@Path("logout")
public class LogoutService extends SessionHandler{
	private static final Logger LOG = LogManager.getLogger(LogoutService.class);
	
	@Path("process")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public void processLogout() {
		HttpSession session = request.getSession(false);
		if (session == null) {
			LOG.info("ログイン出来ていません");
		} else {
			invalidateSession(session);
			LOG.info("セッションを破棄しました");
		}
		setSendRedirect("../../public/login.html");
	}
	
}
