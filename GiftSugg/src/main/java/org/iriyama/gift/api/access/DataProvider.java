package org.iriyama.gift.api.access;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.iriyama.gift.dao.EntryBrandQuery;
import org.iriyama.gift.dao.EntryPlanQuery;
import org.iriyama.gift.dao.EntryThingQuery;
import org.iriyama.gift.dao.TargetPersonQuery;
import org.iriyama.gift.model.EntryBrandEntity;
import org.iriyama.gift.model.EntryPlanEntity;
import org.iriyama.gift.model.EntryThingEntity;
import org.iriyama.gift.model.TargetPersonEntity;
import org.iriyama.gift.suggest.Tweet;
import org.iriyama.gift.suggest.TwitterHandler;
import org.iriyama.gift.util.SessionHandler;

/**
* DBから必要なデータをJSON形式で返すAPI。
* @author eijis
*/
@Path("find")
public class DataProvider extends SessionHandler{
	private static final Logger LOG = LogManager.getLogger(DataProvider.class);
		
	/**
	 * targetPersonIdから、紐づけされたブランドのリストを返します。
	 * @param targetPersonId 対象人物のID
	 * @return json形式のEntryBrandEntityリスト
	 */
	@Path("entryBrand")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<EntryBrandEntity> findBrand(@QueryParam("targetPersonId") int targetPersonId) {
		//TODO セッションのaccountIdがパラメータのtargetPersonIdと紐づけされているかを確認
		LOG.info("EntryBrandService: id = " + targetPersonId);
		EntryBrandQuery entryBrandQuery = new EntryBrandQuery();
		List<EntryBrandEntity> entryBrandList = entryBrandQuery.findListbyAccountId(targetPersonId);
		if (entryBrandList.isEmpty()) {
			LOG.info("entryBrandList: レコードがありません");
		}
		return entryBrandList;
	}
	
	/**
	 * targetPersonIdから、紐づけされたプランのリストを返します。
	 * @param targetPersonId 対象人物のID
	 * @return json形式のEntryPlanEntityリスト
	 */
	@Path("entryPlan")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<EntryPlanEntity> findPlan(@QueryParam("targetPersonId") int targetPersonId) {
		//TODO セッションのaccountIdがパラメータのtargetPersonIdと紐づけされているかを確認
		LOG.info("EntryPlanService: id = " + targetPersonId);
		EntryPlanQuery entryPlanQuery = new EntryPlanQuery();
		List<EntryPlanEntity> entryPlanList = entryPlanQuery.findListbyAccountId(targetPersonId);
		if (entryPlanList.isEmpty()) {
			LOG.info("entryPlanList: レコードがありません");
		}
		return entryPlanList;
	}
	
	/**
	 * targetPersonIdから、紐づけされたモノのリストを返します。
	 * @param targetPersonId 対象人物のID
	 * @return json形式のEntryThingEntityリスト
	 */
	@Path("entryThing")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<EntryThingEntity> findThing(@QueryParam("targetPersonId") @NotNull int targetPersonId) {
		//TODO セッションのaccountIdがパラメータのtargetPersonIdと紐づけされているかを確認
		LOG.info("EntryThingService: id = " + targetPersonId);
		EntryThingQuery entryThingQuery = new EntryThingQuery();
		List<EntryThingEntity> entryThingList = entryThingQuery.findListbyAccountId(targetPersonId);
		if (entryThingList.isEmpty()) {
			LOG.info("entryThingList: レコードがありません");
		}
		return entryThingList;
	}
	
	/**
	 * accountIdから、紐づけされているターゲットのリストを返します。
	 * @return　json形式のTargetPersonEntityリスト
	 */
	@Path("targetPerson")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<TargetPersonEntity> findTargetPerson() {
		//TODO accountIdがDBに格納されていることを確認
		TargetPersonQuery targetPersonQuery = new TargetPersonQuery();
		//TODO accountIdが0だった場合、error.htmlページへリダイレクト
		int accountId = getAccountId();
		List<TargetPersonEntity> targetPersonList = targetPersonQuery.findListByAccountId(accountId);
		if (targetPersonList.isEmpty()) {
			LOG.info("targetPersonList: レコードがありません");
		}
		return targetPersonList;
	}
	
	@Path("tweets")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Tweet> findTweets() {
		TwitterHandler twitterHandler = new TwitterHandler();
		return twitterHandler.getTweets();
	}
}
