package org.iriyama.gift.api.access;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.iriyama.gift.dao.EntryBrandQuery;
import org.iriyama.gift.dao.EntryPlanQuery;
import org.iriyama.gift.dao.EntryThingQuery;
import org.iriyama.gift.dao.TargetPersonQuery;
import org.iriyama.gift.model.EntryBrandEntity;
import org.iriyama.gift.model.EntryPlanEntity;
import org.iriyama.gift.model.EntryThingEntity;
import org.iriyama.gift.model.TargetPersonEntity;
import org.iriyama.gift.util.SessionHandler;

/**
* DBから必要なデータをJSON形式で返すAPI。
* @author eijis
*/
@Path("remove")
public class DataRemover extends SessionHandler{
	private static final Logger LOG = LogManager.getLogger(DataRemover.class);
	
	@Path("entryThing")
	@GET
	public void removeThing (@QueryParam("entryThingId") int entryThingId) {
		LOG.info("entryThingId: " + entryThingId);
		int accountId = getAccountId();

		TargetPersonQuery targetPersonQuery = new TargetPersonQuery();
		List<TargetPersonEntity> targetPersonList = targetPersonQuery.findListByAccountId(accountId);
		if (targetPersonList.isEmpty()) {
			LOG.info("targetPersonList: レコードがありません");
		}
		
		EntryThingQuery entryThingQuery = new EntryThingQuery();
		EntryThingEntity  entryThingEntity = entryThingQuery.getEntityManager().find(EntryThingEntity.class, entryThingId);
		if (entryThingEntity == null) {
			LOG.info("対象のモノが見つかりません");
			return;
		}
		
		for (TargetPersonEntity targetPersonEntity : targetPersonList) {
			if (targetPersonEntity.getTarget_person_id() == entryThingEntity.getTarget_person_id()) {
				LOG.info("delete " + entryThingId + ", targetPersonId: " + entryThingEntity.getTarget_person_id());
				entryThingQuery.delete(entryThingEntity);
				setSendRedirect("../../user/target_person.html?tid=" + entryThingEntity.getTarget_person_id());
				return;
			}
		}
		//TODO 失敗時にエラーページに飛ばす
	}
	
	@Path("entryBrand")
	@GET
	public void removeBrand (@QueryParam("entryBrandId") int entryBrandId) {
		LOG.info("entryBrandId: " + entryBrandId);
		int accountId = getAccountId();

		TargetPersonQuery targetPersonQuery = new TargetPersonQuery();
		List<TargetPersonEntity> targetPersonList = targetPersonQuery.findListByAccountId(accountId);
		if (targetPersonList.isEmpty()) {
			LOG.info("targetPersonList: レコードがありません");
		}
		
		EntryBrandQuery entryBrandQuery = new EntryBrandQuery();
		EntryBrandEntity  entryBrandEntity = entryBrandQuery.getEntityManager().find(EntryBrandEntity.class, entryBrandId);
		if (entryBrandEntity == null) {
			LOG.info("対象のモノが見つかりません");
			return;
		}
		
		for (TargetPersonEntity targetPersonEntity : targetPersonList) {
			if (targetPersonEntity.getTarget_person_id() == entryBrandEntity.getTarget_person_id()) {
				LOG.info("delete " + entryBrandId + ", targetPersonId: " + entryBrandEntity.getTarget_person_id());
				entryBrandQuery.delete(entryBrandEntity);
				setSendRedirect("../../user/target_person.html?tid=" + entryBrandEntity.getTarget_person_id());
				return;
			}
		}
		//TODO 失敗時にエラーページに飛ばす
	}
	
	@Path("entryPlan")
	@GET
	public void removePlan (@QueryParam("entryPlanId") int entryPlanId) {
		LOG.info("entryPlanId: " + entryPlanId);
		int accountId = getAccountId();

		TargetPersonQuery targetPersonQuery = new TargetPersonQuery();
		List<TargetPersonEntity> targetPersonList = targetPersonQuery.findListByAccountId(accountId);
		if (targetPersonList.isEmpty()) {
			LOG.info("targetPersonList: レコードがありません");
		}
		
		EntryPlanQuery entryPlanQuery = new EntryPlanQuery();
		EntryPlanEntity  entryPlanEntity = entryPlanQuery.getEntityManager().find(EntryPlanEntity.class, entryPlanId);
		if (entryPlanEntity == null) {
			LOG.info("対象のモノが見つかりません");
			return;
		}
		
		for (TargetPersonEntity targetPersonEntity : targetPersonList) {
			if (targetPersonEntity.getTarget_person_id() == entryPlanEntity.getTarget_person_id()) {
				LOG.info("delete " + entryPlanId + ", targetPersonId: " + entryPlanEntity.getTarget_person_id());
				entryPlanQuery.delete(entryPlanEntity);
				setSendRedirect("../../user/target_person.html?tid=" + entryPlanEntity.getTarget_person_id());
				return;
			}
		}
		//TODO 失敗時にエラーページに飛ばす
	}
}