package org.iriyama.gift.api.access;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.iriyama.gift.dao.EntryBrandQuery;
import org.iriyama.gift.dao.EntryPlanQuery;
import org.iriyama.gift.dao.EntryThingQuery;
import org.iriyama.gift.model.EntryBrandEntity;
import org.iriyama.gift.model.EntryPlanEntity;
import org.iriyama.gift.model.EntryThingEntity;
import org.iriyama.gift.util.SessionHandler;

/**
* DBへデータを格納するAPI。
* @author eijis
*/
@Path("add")
public class DataAdder extends SessionHandler{
	private static final Logger LOG = LogManager.getLogger(DataAdder.class);

	/**
	 * targetPersonIdから、ブランド情報をDBに格納します。
	 * @param targetPersonId
	 * @param description
	 */
	@Path("entryBrand")
	@GET
	public void addBrand(@QueryParam("targetPersonId") @NotNull int targetPersonId,
						@QueryParam("description") @NotNull String description) {
		LOG.info("EntryBrandに" + description + "を追加します");
		EntryBrandEntity entryBrandEitity = new EntryBrandEntity();
		entryBrandEitity.setTarget_person_id(targetPersonId);
		entryBrandEitity.setDescription(description);

		EntryBrandQuery entryBrandQuery = new EntryBrandQuery();
		entryBrandQuery.insert(entryBrandEitity);

		setSendRedirect("../../user/target_person.html?tid=" + targetPersonId);
		return;
	}
	
	/**
	 * targetPersonIdから、プラン情報をDBに格納します。
	 * @param targetPersonId
	 * @param description
	 */
	@Path("entryPlan")
	@GET
	public void addPlan(@QueryParam("targetPersonId") @NotNull int targetPersonId,
						@QueryParam("description") @NotNull String description) {
		LOG.info("EntryPlanに" + description + "を追加します");
		EntryPlanEntity entryPlanEntity = new EntryPlanEntity();
		entryPlanEntity.setTarget_person_id(targetPersonId);
		entryPlanEntity.setDescription(description);

		EntryPlanQuery entryPlanQuery = new EntryPlanQuery();
		entryPlanQuery.insert(entryPlanEntity);
		
		setSendRedirect("../../user/target_person.html?tid=" + targetPersonId);
	}
	
	/**
	 * targetPersonIdから、モノ情報をDBに格納します。
	 * @param targetPersonId
	 * @param description
	 */
	@Path("entryThing")
	@GET
	public void addThing(@QueryParam("targetPersonId") @NotNull int targetPersonId,
						@QueryParam("description") String description) {
		LOG.info("EntryThingに" + description + "を追加します");
		EntryThingEntity entryThingEntity = new EntryThingEntity();
		entryThingEntity.setTarget_person_id(targetPersonId);
		entryThingEntity.setDescription(description);

		EntryThingQuery entryThingQuery = new EntryThingQuery();
		entryThingQuery.insert(entryThingEntity);

		setSendRedirect("../../user/target_person.html?tid=" + targetPersonId);
		return;
	}
	
	/**
	 * targetPersonIdから、ターゲット人物情報をDBに格納します。
	 * @param targetPersonId
	 * @param description
	 */
	@Path("targetPerson")
	@GET
	public void addTargetPerson(@QueryParam("targetPersonId") @NotNull int targetPersonId,
						@QueryParam("description") String description) {
		LOG.info("EntryThingに" + description + "を追加します");
		EntryThingEntity entryThingEntity = new EntryThingEntity();
		entryThingEntity.setTarget_person_id(targetPersonId);
		entryThingEntity.setDescription(description);

		EntryThingQuery entryThingQuery = new EntryThingQuery();
		entryThingQuery.insert(entryThingEntity);

		setSendRedirect("../../user/target_person.html?tid=" + targetPersonId);
		return;
	}
}
