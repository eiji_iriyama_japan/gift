package org.iriyama.gift.api.auth;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.iriyama.gift.dao.AccountQuery;
import org.iriyama.gift.model.AccountEntity;
import org.iriyama.gift.util.SessionHandler;

/**
 * サインアップ処理をします。
 * @author eijis
 *
 */
@Path("signup")
public class SignUpService extends SessionHandler{
	private static final Logger LOG = LogManager.getLogger(SignUpService.class);
	
	@Path("check")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public void signup(@QueryParam("firstname") String firstname,
						@QueryParam("lastname") String lastname,
						@QueryParam("sex") String sex,
						@QueryParam("email") String email,
						@QueryParam("birthday") String birthday,
						@QueryParam("password") String password,
						@QueryParam("reenter_password") String reenter_password) {
		if ((firstname == null || "".equals(firstname)) ||
				(lastname == null || "".equals(lastname)) ||
				(sex == null || "".equals(sex)) ||
				(email == null || "".equals(email)) ||
				(birthday == null || "".equals(birthday)) ||
				(password == null || "".equals(password)) ||
				(reenter_password == null || "".equals(reenter_password))
				) {
			LOG.info("空の入力項目があります");
			setSendRedirect("../../public/signup_error.html");
		}
		
		if (!password.equals(reenter_password)) {
			LOG.info("パスワードと再入力したパスワードが一致しません");
			setSendRedirect("../../public/signup_error.html");
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date birthdayInFormat = null;
		try {
			birthdayInFormat = simpleDateFormat.parse(birthday);
		} catch (ParseException e) {
			LOG.error("誕生日を変換できませんでした");
		}
		//TODO 同じメールアドレスでアカウントが登録されていないkとを確認する
		AccountEntity accountEntity = new AccountEntity();
		accountEntity.setFirstname(firstname);
		accountEntity.setLastname(lastname);
		accountEntity.setSex(sex);
		accountEntity.setEmail(email);
		accountEntity.setBirthday(birthdayInFormat);
		accountEntity.setPassword(password);
		AccountQuery accountQuery = new AccountQuery();
		accountQuery.insert(accountEntity);
		
		LOG.info("ログインに成功しました。");
		HttpSession session = request.getSession(true);
		session.setAttribute(USER_SESSION, accountEntity.getAccount_id());
		setSendRedirect("../../user/index.html");
	}
}
