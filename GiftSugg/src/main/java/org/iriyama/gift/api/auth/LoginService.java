package org.iriyama.gift.api.auth;

import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.iriyama.gift.dao.AccountQuery;
import org.iriyama.gift.model.AccountEntity;
import org.iriyama.gift.util.SessionHandler;

/**
 * ログイン処理をします<br>
 * 成功時にindex.htmlへ遷移<br>
 * 失敗時にerror.htmlへ遷移して、再度ログインをしてもらう。
 * @author eijis
 *
 */
@Path("login")
public class LoginService extends SessionHandler {
	private static final Logger LOG = LogManager.getLogger(LoginService.class);
	
	@Path("check")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public void check(@QueryParam("email") String email,
						@QueryParam("password") String password) {
		AccountQuery accountQuery = new AccountQuery();
		AccountEntity accountEntity = accountQuery.findListByEmail(email);
		
		if (accountEntity == null) {
			LOG.info("メールアドレスが見つかりません");
			setSendRedirect("../../public/login_error.html");
			return;
		}
		
		if (!accountEntity.getPassword().equals(password)) {
			LOG.info("パスワードが間違っております。");
			setSendRedirect("../../public/login_error.html");
			return;
		} else {
			LOG.info("ログインに成功しました。");
			HttpSession session = request.getSession(true);
			session.setAttribute(USER_SESSION, accountEntity.getAccount_id());
			setSendRedirect("../../user/index.html");
			return;
		}
	}
}
