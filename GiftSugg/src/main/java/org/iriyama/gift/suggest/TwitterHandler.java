package org.iriyama.gift.suggest;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class TwitterHandler {
	private static final Logger LOG = LogManager.getLogger(TwitterHandler.class);
	private final String searchKeyword= "プレゼント　嬉しい -企画 -アンケート -キャンペーン -抽選";
	private final int TWEET_COUNT = 20;
	
	public List<Tweet> getTweets() {
		LOG.info("TweetService");
		Twitter twitter = TwitterFactory.getSingleton();

		Query query = new Query(searchKeyword);
		query.setCount(TWEET_COUNT);
		QueryResult queryResult = null;
		try {
			queryResult = twitter.search(query);
		} catch (TwitterException e) {
			LOG.error("ツイートを取得できませんでした");
			//TODO エラーメッセージを返す
		}
		
		List<Tweet> tweetList = new ArrayList<Tweet>();
		for (Status status : queryResult.getTweets()) {
			Tweet tweet = new Tweet();
			tweet.setName(status.getUser().getName());
			tweet.setText(status.getText());
			tweetList.add(tweet);
		}
		return tweetList;
	}
}
