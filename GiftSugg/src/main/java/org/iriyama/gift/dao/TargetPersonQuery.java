package org.iriyama.gift.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.iriyama.gift.model.TargetPersonEntity;

/**
 * TargetPersonテーブルへの処理をします。
 * @author eijis
 *
 */
public class TargetPersonQuery extends Query<TargetPersonEntity>{
	public TargetPersonQuery() {
		super();
	}

	public List<TargetPersonEntity> findListByAccountId(int accountId) {
		TypedQuery<TargetPersonEntity> query = getEntityManager().createNamedQuery("TargetPersonEntity.findListByAccountId", TargetPersonEntity.class);
		query.setParameter("accountId", accountId);
		List<TargetPersonEntity> targetPersonList = query.getResultList();

		return targetPersonList;
	}
}
