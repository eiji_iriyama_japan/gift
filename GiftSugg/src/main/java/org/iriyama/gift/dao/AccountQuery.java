package org.iriyama.gift.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.iriyama.gift.model.AccountEntity;

/**
 * Accountテーブルへの処理をします。
 * @author eijis
 *
 */
public class AccountQuery extends Query<AccountEntity>{
	private static final Logger LOG = LogManager.getLogger(AccountQuery.class);
	
	public AccountQuery() {
	}

	public List<AccountEntity> findList(int accountId) {
		TypedQuery<AccountEntity> query = getEntityManager().createNamedQuery("AccountEntity.findList", AccountEntity.class);
		List<AccountEntity> accountList = query.getResultList();

		return accountList;
	}
	
	public AccountEntity findListByEmail(String email) {
		TypedQuery<AccountEntity> query = getEntityManager().createNamedQuery("AccountEntity.findListByEmail", AccountEntity.class);
		query.setParameter("email", email);
		AccountEntity accountEntity = null;
		try {
			accountEntity = query.getSingleResult();
		} catch (NoResultException e) {
			accountEntity = null;
			LOG.error(e.getMessage(), e);
		}
		return accountEntity;
	}
}
