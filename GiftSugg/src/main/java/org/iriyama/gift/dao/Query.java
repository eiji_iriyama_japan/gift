package org.iriyama.gift.dao;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * テーブルへの処理を行うQueryクラスのひな型<br>
 * 基本的なCRUDを実装。
 * @author eijis
 *
 * @param <T> 対象のEntityクラス
 */
public class Query<T> {
	private static final Logger LOG = LogManager.getLogger(Query.class);
	
	@PersistenceContext(unitName = "giftPU")
	private EntityManager entityManager;
	
	public Query() {
		entityManager = Persistence.createEntityManagerFactory("giftPU").createEntityManager();
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	/* ジェネリックのクラス型を返せない
	public T find(int id) {
		return (T) entityManager.find(Query.class.getGenericInterfaces().getClass(), id);
	}
	*/
	
	public void insert(T entity){
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();
	}
	
	public void delete(T entity){
		entityManager.getTransaction().begin();
		entityManager.remove(entity);
		entityManager.getTransaction().commit();
	}
	
	public void update(T entity){
		//entityManager.getTransaction().begin();
		//entityManager.getTransaction().commit();
	}
}