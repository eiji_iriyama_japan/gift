package org.iriyama.gift.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.iriyama.gift.model.EntryBrandEntity;

/**
 * EntryBrandテーブルへの処理をします。
 * @author eijis
 *
 */
public class EntryBrandQuery extends Query<EntryBrandEntity>{

	public EntryBrandQuery() {
		super();
	}
	
	public List<EntryBrandEntity> findListbyAccountId(int targetPersonId) {
		TypedQuery<EntryBrandEntity> query = getEntityManager().createNamedQuery("EntryBrandEntity.findListbyTargetPersonId", EntryBrandEntity.class);
		query.setParameter("targetPersonId", targetPersonId);
		List<EntryBrandEntity> entryBrandEntityList = query.getResultList();

		return entryBrandEntityList;
	}
}
