package org.iriyama.gift.dao;

import java.util.List;
import javax.persistence.TypedQuery;

import org.iriyama.gift.model.EntryThingEntity;

/**
 * EntryThingテーブルへの処理をします。
 * @author eijis
 *
 */
public class EntryThingQuery extends Query<EntryThingEntity>{
	public EntryThingQuery() {
		super();
	}
	
	public List<EntryThingEntity> findListbyAccountId(int targetPersonId) {
		TypedQuery<EntryThingEntity> query = getEntityManager().createNamedQuery("EntryThingEntity.findListbyTargetPersonId", EntryThingEntity.class);
		query.setParameter("targetPersonId", targetPersonId);
		List<EntryThingEntity> entryThingEntityList = query.getResultList();

		return entryThingEntityList;
	}
}
