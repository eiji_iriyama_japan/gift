package org.iriyama.gift.dao;

import java.util.List;
import javax.persistence.TypedQuery;

import org.iriyama.gift.model.EntryPlanEntity;

/**
 * EntryPlanテーブルへの処理をします。
 * @author eijis
 *
 */
public class EntryPlanQuery extends Query<EntryPlanEntity>{
	
	public EntryPlanQuery() {
		super();
	}
	
	public List<EntryPlanEntity> findListbyAccountId(int targetPersonId) {
		TypedQuery<EntryPlanEntity> query = getEntityManager().createNamedQuery("EntryPlanEntity.findListbyTargetPersonId", EntryPlanEntity.class);
		query.setParameter("targetPersonId", targetPersonId);
		List<EntryPlanEntity> entryPlanEntityList = query.getResultList();

		return entryPlanEntityList;
	}
}
