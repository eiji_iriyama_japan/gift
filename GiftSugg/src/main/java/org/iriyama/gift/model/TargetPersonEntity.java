package org.iriyama.gift.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * target_personテーブルのEntityを定義。
 * @author eijis
 *
 */
@NamedQueries({
	@NamedQuery(name = "TargetPersonEntity.findListByAccountId", query = "select e from TargetPersonEntity e where account_id =:accountId")
})
@Entity
@Table(name = "target_person")
public class TargetPersonEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int target_person_id;
	private int account_id;
	private String firstname;
	private String lastname;
	private String sex;
	private String email;
	private Date birthday;
	public int getTarget_person_id() {
		return target_person_id;
	}
	public void setTarget_person_id(int target_person_id) {
		this.target_person_id = target_person_id;
	}
	public int getAccount_id() {
		return account_id;
	}
	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
}
