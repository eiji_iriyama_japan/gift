package org.iriyama.gift.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * 未使用<br>
 * entry_imgテーブルのEntityを定義。
 * @author eijis
 *
 */
@NamedQueries({
	@NamedQuery(name = "EntryImgEntity.findListbyTargetPersonId", query = "select e from EntryImgEntity e where target_person_id =:targetPersonId")
})
@Entity
@Table(name = "entry_img")
public class EntryImgEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int entry_img_id;
	private int target_person_id;
	private String img_name;
	public int getEntry_img_id() {
		return entry_img_id;
	}
	public void setEntry_img_id(int entry_img_id) {
		this.entry_img_id = entry_img_id;
	}
	public int getTarget_person_id() {
		return target_person_id;
	}
	public void setTarget_person_id(int target_person_id) {
		this.target_person_id = target_person_id;
	}
	public String getImg_name() {
		return img_name;
	}
	public void setImg_name(String img_name) {
		this.img_name = img_name;
	}
}
