package org.iriyama.gift.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * entry_planテーブルのEntityを定義。
 * @author eijis
 *
 */
@NamedQueries({
	@NamedQuery(name = "EntryPlanEntity.findListbyTargetPersonId", query = "select e from EntryPlanEntity e where target_person_id =:targetPersonId")
})
@Entity
@Table(name = "entry_plan")
public class EntryPlanEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int entry_plan_id;
	private int target_person_id;
	private Date target_date;
	private String title;
	private String description;
	private Boolean done;
	public int getEntry_plan_id() {
		return entry_plan_id;
	}
	public void setEntry_plan_id(int entry_plan_id) {
		this.entry_plan_id = entry_plan_id;
	}
	public int getTarget_person_id() {
		return target_person_id;
	}
	public void setTarget_person_id(int target_person_id) {
		this.target_person_id = target_person_id;
	}
	public Date getTarget_date() {
		return target_date;
	}
	public void setTarget_date(Date target_date) {
		this.target_date = target_date;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getDone() {
		return done;
	}
	public void setDone(Boolean done) {
		this.done = done;
	}
}
