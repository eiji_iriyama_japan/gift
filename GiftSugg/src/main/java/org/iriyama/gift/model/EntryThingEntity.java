package org.iriyama.gift.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * entry_thingテーブルのEntityを定義。
 * @author eijis
 *
 */
@NamedQueries({
	@NamedQuery(name = "EntryThingEntity.findListbyTargetPersonId", query = "select e from EntryThingEntity e where target_person_id =:targetPersonId")
})
@Entity
@Table(name = "entry_thing")
public class EntryThingEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int entry_thing_id;
	private int target_person_id;
	private String description;
	public int getEntry_thing_id() {
		return entry_thing_id;
	}
	public void setEntry_thing_id(int entry_thing_id) {
		this.entry_thing_id = entry_thing_id;
	}
	public int getTarget_person_id() {
		return target_person_id;
	}
	public void setTarget_person_id(int target_person_id) {
		this.target_person_id = target_person_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
