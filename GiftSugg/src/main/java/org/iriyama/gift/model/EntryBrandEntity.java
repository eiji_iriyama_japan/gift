package org.iriyama.gift.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * entry_brandテーブルのEntityを定義。
 * @author eijis
 *
 */
@NamedQueries({
	@NamedQuery(name = "EntryBrandEntity.findListbyTargetPersonId", query = "select e from EntryBrandEntity e where target_person_id =:targetPersonId")
})
@Entity
@Table(name = "entry_brand")
public class EntryBrandEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int entry_brand_id;
	private int target_person_id;
	private String description;
	public int getEntry_brand_id() {
		return entry_brand_id;
	}
	public void setEntry_brand_id(int entry_brand_id) {
		this.entry_brand_id = entry_brand_id;
	}
	public int getTarget_person_id() {
		return target_person_id;
	}
	public void setTarget_person_id(int target_person_id) {
		this.target_person_id = target_person_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
