package org.iriyama.gift.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * 未使用<br>
 * entry_colorテーブルのEntityを定義。
 * @author eijis
 *
 */
@NamedQueries({
	@NamedQuery(name = "EntryColorEntity.findListbyTargetPersonId", query = "select e from EntryColorEntity e where target_person_id =:targetPersonId")
})
@Entity
@Table(name = "entry_color")
public class EntryColorEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int entry_color_id;
	private int target_person_id;
	private String description;
	public int getEntry_color_id() {
		return entry_color_id;
	}
	public void setEntry_color_id(int entry_color_id) {
		this.entry_color_id = entry_color_id;
	}
	public int getTarget_person_id() {
		return target_person_id;
	}
	public void setTarget_person_id(int target_person_id) {
		this.target_person_id = target_person_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
